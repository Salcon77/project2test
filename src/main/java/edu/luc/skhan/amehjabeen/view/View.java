/*package edu.luc.skhan.amehjabeen.view;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.luc.skhan.amehjabeen.common.Constants;
import edu.luc.skhan.amehjabeen.model.classes.Manager;
import edu.luc.skhan.amehjabeen.model.facility.IFacility;
import edu.luc.skhan.amehjabeen.model.facility.IInformation;
import edu.luc.skhan.amehjabeen.model.maintenance.IProblem;

public class View {
	public static void main(String args[]){
		
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		System.out.println("***************** Application Context instantiated! ******************");
		
		Manager manager = (Manager)context.getBean("manager");
		manager.setPersonId("XYZ123");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		System.out.println(" Adding the first facility....");
		IFacility facility = (IFacility)context.getBean("facility");
		facility.setName("Science Hall");
		facility.setId(1);
		
		IInformation information = (IInformation) facility.getInformation();
		information.setTotalCapacity(10);
		information.setAvailableCapacity(10);
		information.setStatus(Constants.STATUS_AVAILABLE);
		information.setDescription("Hello World");
		
		  System.out.println("*************** Saving Facility ***********************");
	      manager.addNewFacility(facility);
	      System.out.println("*************** Facility Inserted *************************");
		
	      IFacility facility2 = (IFacility) manager.retrieveFacility(1);
	      
	     try{
	
		System.out.println("Printing the first facility's information: \n"
				+"Facility Name: "+facility2.getName()+"\n"
				+"Total Capacity: "+facility2.getInformation().getTotalCapacity()+"\n"
				//+"Available Capacity: "+manager.requestAvailableCapacity(facility)+"\n"
				+"Description: "+facility2.getInformation().getDescription()+"\n"
				+"Status: "+facility2.getInformation().getStatus()+"\n");
	     }catch(Exception e){
	    	 e.printStackTrace();
	     }

		System.out.println(" Adding a second facility....");
		
		IFacility facility3 = (IFacility)context.getBean("facility");
		facility3.setName("Math Hall");
		
		IInformation information2 = facility3.getInformation();
		information2.setTotalCapacity(21);
		information2.setStatus(Constants.STATUS_AVAILABLE);
		information2.setDescription("This is our second facility");
		
		manager.addNewFacility(facility3);
		manager.removeFacility(facility);
		
		String type = "cooling";
		String maintenanceRequestStatus = Constants.MAINTENANCE_NOT_REQUESTED;
		IProblem problem = (IProblem)context.getBean("problem");
		problem.setTypeOfProblem(type);
		problem.setMaintenanceRequestStatus(maintenanceRequestStatus);
		problem.setFacility(facility3);
		manager.addFacilityProblem(problem);
		
		
		
	}
 
 
}
*/