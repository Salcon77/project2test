package edu.luc.skhan.amehjabeen.data;

import edu.luc.skhan.amehjabeen.model.facility.IFacility;
import edu.luc.skhan.amehjabeen.model.use.IUse;

public interface IHibernate {

	void addFacility(IFacility facility);

	void removeFacility(IFacility facility);

	IFacility retrieveFacility(long id);
	
	public void assignFacilityToUse(IUse use);

}
