package edu.luc.skhan.amehjabeen.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.luc.skhan.amehjabeen.model.facility.IFacility;
import edu.luc.skhan.amehjabeen.model.maintenance.IMaintenance;
import edu.luc.skhan.amehjabeen.model.maintenance.IMaintenanceRequest;
import edu.luc.skhan.amehjabeen.model.maintenance.IProblem;
import edu.luc.skhan.amehjabeen.model.use.IInspections;
import edu.luc.skhan.amehjabeen.model.use.IUse;
import edu.luc.skhan.amehjabeen.model.use.Inspections;


public class HibernateMethods implements IDataModel{

	private static HibernateMethods instance;

	public static HibernateMethods getInstance(){
		if(instance == null){
			instance = new HibernateMethods();
		}
		return instance;
	}

	
	@Override
	public void removeFacility(IFacility facility){
		System.out.println("*************** Deleteing facility information in DB with ID ...  " + facility.getId());
		Session session = HibernateHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(facility);
		session.getTransaction().commit();
	}
	
	@Override
	public IFacility retrieveFacility(long id){
		try {
			System.out.println("*************** Searcing for facility with ID ...  " + id);
			Session session = HibernateHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			Query getFacilityQuery = session.createQuery("From Facility where id=:id");		
			getFacilityQuery.setLong("id", id);
			
			System.out.println("*************** Retrieve Query is ....>>\n" + getFacilityQuery.toString()); 
			
			List facilities = getFacilityQuery.list();
			System.out.println("Getting Facility Details using HQL. \n" + facilities);

			session.getTransaction().commit();
			return (IFacility)facilities.get(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
	}

	@Override
	public void assignFacilityToUse(IUse use) {
		Session session = HibernateHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(use);
		session.getTransaction().commit();
		
	}

	@Override
	public void addNewFacility(IFacility facility) {
		Session session = HibernateHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(facility);
		session.getTransaction().commit();
	}


	@Override
	public ArrayList<IFacility> listFacilities() {
		try {
			Session session = HibernateHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			Query getFacilityQuery = session.createQuery("From Facility");				
			System.out.println("*************** Retrieve Query is ....>>\n" + getFacilityQuery.toString()); 
			ArrayList<IFacility> facilities = (ArrayList<IFacility>) getFacilityQuery.list();
			System.out.println("Getting Facility Details using HQL. \n" + facilities);
			session.getTransaction().commit();
			return (ArrayList<IFacility>)facilities;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
	}

	@Override
	public void addFacilityDetail(IFacility facility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeFacilityDetail(IFacility facility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int requestAvailableCapacity(IFacility facility) {
		return 0;
	}

	@Override
	public void addFacilityProblem(IProblem problem) {
		Session session = HibernateHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(problem);
		session.getTransaction().commit();		
	}

	@Override
	public double calcProblemRateForFacility(IFacility facility) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<IProblem> listFacilityProblems(IFacility facility) {
		try {
			Session session = HibernateHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			Query getFacilityQuery = session.createQuery("From Problems where facilityId=: facilityid ");		
			getFacilityQuery.setLong("facilityid", facility.getId());
			System.out.println("*************** Retrieve Query is ....>>\n" + getFacilityQuery.toString()); 
			ArrayList<IProblem> problems = (ArrayList<IProblem>) getFacilityQuery.list();
			System.out.println("Getting Facility Details using HQL. \n" + problems);
			session.getTransaction().commit();
			return (ArrayList<IProblem>)problems;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
	}

	@Override
	public double calcMaintenanceCostForFacility(IMaintenance maintenance) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<IMaintenance> listMaintenance() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void scheduleMaintenance(IMaintenance maintenance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void makeFacilityMaintenanceRequest(
			IMaintenanceRequest maintenanceRequest) {
		Session session = HibernateHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(maintenanceRequest);
		session.getTransaction().commit();
		
	}

	@Override
	public ArrayList<IMaintenanceRequest> listMaintenanceRequests() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isInUseDuringInterval(IFacility facility, Date startTime,
			Date endTime) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void vacateFacility(IFacility facility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<IUse> listActualUsage(IFacility facility) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double calcUsageRate(IFacility facility) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void changeIsInUseStatus(IUse use) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Inspections> listInspections() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addInspections(IInspections inspections) {
		Session session = HibernateHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(inspections);
		session.getTransaction().commit();
		
	}

}
