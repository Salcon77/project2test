package edu.luc.skhan.amehjabeen.data;

import edu.luc.skhan.amehjabeen.model.facility.IFacilityBasics;
import edu.luc.skhan.amehjabeen.model.facility.IInformationBasics;
import edu.luc.skhan.amehjabeen.model.maintenance.IMaintenanceBasics;
import edu.luc.skhan.amehjabeen.model.maintenance.IMaintenanceRequestBasics;
import edu.luc.skhan.amehjabeen.model.maintenance.IProblemBasics;
import edu.luc.skhan.amehjabeen.model.use.IInspectionsBasic;
import edu.luc.skhan.amehjabeen.model.use.IUseBasic;

public interface IDataModel extends IFacilityBasics,IInformationBasics,
									IProblemBasics,IMaintenanceBasics,
									IMaintenanceRequestBasics,IUseBasic,IInspectionsBasic {

}
