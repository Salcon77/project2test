package edu.luc.skhan.amehjabeen.model.classes;

import java.util.ArrayList;
import java.util.Date;

import edu.luc.skhan.amehjabeen.data.HibernateMethods;
import edu.luc.skhan.amehjabeen.data.IDataModel;
import edu.luc.skhan.amehjabeen.model.facility.IFacility;
import edu.luc.skhan.amehjabeen.model.maintenance.IMaintenance;
import edu.luc.skhan.amehjabeen.model.maintenance.IMaintenanceRequest;
import edu.luc.skhan.amehjabeen.model.maintenance.IProblem;
import edu.luc.skhan.amehjabeen.model.use.IInspections;
import edu.luc.skhan.amehjabeen.model.use.IUse;
import edu.luc.skhan.amehjabeen.model.use.Inspections;

public class Manager extends Person implements IDataModel{
	
	public Manager(){}
	
	HibernateMethods hibernateMethods = HibernateMethods.getInstance();

	public void addNewFacility(IFacility facility) {
		try {
			hibernateMethods.addNewFacility(facility);
	    } catch (Exception se) {
	      System.err.println("Manager: Threw a Exception while adding facility.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	    }
	}

	//@Override
	public void removeFacility(IFacility facility) {
		try {
			hibernateMethods.removeFacility(facility);
	    } catch (Exception se) {
	      System.err.println("Manager: Threw a Exception while removing facility.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	    }	
	}

	//@Override
	public IFacility retrieveFacility(long id) {
		try {
			return hibernateMethods.retrieveFacility(id);
	    } catch (Exception se) {
	      System.err.println("Manager: Threw a Exception while removing facility.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	      return null;
	    }	
	}

	@Override
	public ArrayList<IFacility> listFacilities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addFacilityDetail(IFacility facility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeFacilityDetail(IFacility facility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int requestAvailableCapacity(IFacility facility) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void addFacilityProblem(IProblem problem) {
		try {
			hibernateMethods.addFacilityProblem(problem);
	    } catch (Exception se) {
	      System.err.println("Manager: Threw a Exception while removing facility.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	    }
	}

	@Override
	public double calcProblemRateForFacility(IFacility facility) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<IProblem> listFacilityProblems(IFacility facility) {
		try {
			return hibernateMethods.listFacilityProblems(facility);
	    } catch (Exception se) {
	      System.err.println("Manager: Threw a Exception while removing facility.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	      return null;
	    }	
	}

	@Override
	public double calcMaintenanceCostForFacility(IMaintenance maintenance) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<IMaintenance> listMaintenance() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void scheduleMaintenance(IMaintenance maintenance) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void makeFacilityMaintenanceRequest(
			IMaintenanceRequest maintenanceRequest) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<IMaintenanceRequest> listMaintenanceRequests() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isInUseDuringInterval(IFacility facility, Date startTime,
			Date endTime) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void assignFacilityToUse(IUse use) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void vacateFacility(IFacility facility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<IUse> listActualUsage(IFacility facility) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double calcUsageRate(IFacility facility) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void changeIsInUseStatus(IUse use) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Inspections> listInspections() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addInspections(IInspections inspections) {
		// TODO Auto-generated method stub
		
	}



	/*@Override
	public void makeFacilityMaintenanceRequest(IMaintenanceRequest maintenanceRequest) {
		hibernateMethods.makeFacilityMaintenanceRequest(maintenanceRequest);		
	}

	@Override
	public ArrayList<IMaintenanceRequest> listMaintenanceRequests() {
		return hibernateMethods.listMaintenanceRequests();
	}
	

	@Override
	public ArrayList<IMaintenance> listMaintenance() {
		return hibernateMethods.listMaintenance();
	}

	@Override
	public void scheduleMaintenance(IMaintenance maintenance) {
		hibernateMethods.scheduleMaintenance(maintenance);	
	}

	@Override
	public double calcMaintenanceCostForFacility(IMaintenance maintenance) {
		return hibernateMethods.calcMaintenanceCostForFacility(maintenance);				
	}

	@Override
	public ArrayList<IProblem> listFacilityProblems(IFacility facility) {
		return hibernateMethods.listFacilityProblems(facility);
	}
	
	@Override
	public double calcProblemRateForFacility(IFacility facility) {
		return hibernateMethods.calcProblemRateForFacility(facility);
	}

	@Override
	public void addFacilityProblem(IProblem problem) {
		hibernateMethods.addFacilityProblem(problem);
		
	}

	@Override
	public void addFacilityDetail(IFacility facility) {
		hibernateMethods.addFacilityDetail(facility);
		
	}

	@Override
	public void removeFacilityDetail(IFacility facility) {
		hibernateMethods.removeFacilityDetail(facility);
		
	}

	@Override
	public int requestAvailableCapacity(IFacility facility) {
		return hibernateMethods.requestAvailableCapacity(facility);
	}

	@Override
	public boolean isInUseDuringInterval(IFacility facility,Date startTime, Date endTime) {
		return hibernateMethods.isInUseDuringInterval(facility, startTime, endTime);
	}

	@Override
	public void assignFacilityToUse(IUse use) {
		hibernateMethods.assignFacilityToUse(use);
		
	}

	@Override
	public void vacateFacility(IFacility facility) {
		hibernateMethods.vacateFacility(facility);
		
	}

	@Override
	public ArrayList<Inspections> listInspections() {
		return hibernateMethods.listInspections();
	}

	@Override
	public ArrayList<IUse> listActualUsage(IFacility facility) {
		return hibernateMethods.listActualUsage(facility);
	}

	@Override
	public double calcUsageRate(IFacility facility) {
		return hibernateMethods.calcUsageRate(facility);
	}

	@Override
	public void addInspections(IInspections inspections) {
		hibernateMethods.addInspections(inspections);
		
	}

	@Override
	public void changeIsInUseStatus(IUse use) {
		hibernateMethods.changeIsInUseStatus(use);
		
	}*/
	
}
