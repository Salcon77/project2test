package edu.luc.skhan.amehjabeen.model.facility;

import java.util.ArrayList;

public interface IFacilityBasics {
	public void addNewFacility(IFacility facility);
	public ArrayList<IFacility> listFacilities();
	IFacility retrieveFacility(long id);
	public void removeFacility(IFacility facility);
}
