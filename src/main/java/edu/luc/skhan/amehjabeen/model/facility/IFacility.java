package edu.luc.skhan.amehjabeen.model.facility;


public interface IFacility{
	public long getId();
	public void setId(long id);	
	public IInformation getInformation();
	public void setInformation(IInformation information);
	public String getName();
	public void setName(String name);
}
