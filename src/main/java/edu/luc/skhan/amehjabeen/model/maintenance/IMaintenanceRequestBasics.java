package edu.luc.skhan.amehjabeen.model.maintenance;

import java.util.ArrayList;

public interface IMaintenanceRequestBasics {
	public void makeFacilityMaintenanceRequest(IMaintenanceRequest maintenanceRequest);
	public ArrayList<IMaintenanceRequest> listMaintenanceRequests();
}
